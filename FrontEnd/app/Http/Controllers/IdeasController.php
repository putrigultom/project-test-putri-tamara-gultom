<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IdeasController extends Controller
{
    public function index()
    {
        $apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';
        $params = [
            'page[number]' => 1,
            'page[size]' => 10,
            'append' => ['small_image', 'medium_image'],
            'sort' => '-published_at'
        ];

        try {
            $response = Http::get($apiUrl, $params);
            return $response->json();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500); 
        }
    }
}
