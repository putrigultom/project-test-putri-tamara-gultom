const sidolarPosts = [];

const postData = {
    imageSrc: ["/project/img/box1.jpg", "/project/img/box2.jpg"],
    titles: [
        "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
        "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer ..."
    ]
};

function randomDateInRange(start, end) {
    const startDate = new Date(start);
    const endDate = new Date(end);
    const randomTime = startDate.getTime() + Math.random() * (endDate.getTime() - startDate.getTime());
    return new Date(randomTime);
}

for (let i = 0; i < 100; i++) {
    const titleIndex = i % postData.titles.length;
    const imageIndex = i % postData.imageSrc.length;
    const startDate = new Date(2020, 8, 1);

    const randomDate = randomDateInRange(startDate, new Date(Date.now() + 100 * 24 * 60 * 60 * 1000));

    const post = {
        imageSrc: postData.imageSrc[imageIndex],
        date: randomDate.toLocaleDateString('id-ID', { day: 'numeric', month: 'long', year: 'numeric' }),
        title: postData.titles[titleIndex]
    };


    sidolarPosts.push(post);
}

console.log(sidolarPosts);

function sortPostsByDate(order) {
    sidolarPosts.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);
        return order === 'newest' ? dateB - dateA : dateA - dateB;
    });

    const currentPageButton = document.querySelector('.pagination-btn.active');
    const pageNumber = parseInt(currentPageButton.textContent);
    displayPosts(pageNumber);
}

const sortByDropdown = document.getElementById('sort-by');
sortByDropdown.addEventListener('change', function () {
    const sortOrder = sortByDropdown.value === 'published_at' ? 'oldest' : 'newest';
    sortPostsByDate(sortOrder);
});

const postList = document.getElementById('postList');

function createSidolarPost(post) {
    const postElement = document.createElement('div');
    postElement.classList.add('box');

    const imgElement = document.createElement('img');
    imgElement.src = post.imageSrc;
    imgElement.alt = "Gambar Post";
    postElement.appendChild(imgElement);

    const dateElement = document.createElement('p');
    dateElement.textContent = post.date;
    postElement.appendChild(dateElement);

    const titleElement = document.createElement('h5');
    titleElement.textContent = post.title;
    postElement.appendChild(titleElement);

    postList.appendChild(postElement);
}

function displayPosts(pageNumber, postPer) {
    const postsPerPage = 8;
    const startIndex = (pageNumber - 1) * postsPerPage;
    const endIndex = startIndex + postsPerPage;
    const currentPagePosts = sidolarPosts.slice(startIndex, endIndex);

    postList.innerHTML = '';

    currentPagePosts.forEach(post => {
        createSidolarPost(post);
    });

    updatePaginationButtons(pageNumber);
}

function updatePaginationButtons(currentPage) {
    const paginationContainer = document.querySelector('.pagination-container-down');
    paginationContainer.innerHTML = '';

    const totalPages = Math.ceil(sidolarPosts.length / 8);
    const maxVisibleButtons = 5;
    const maxPages = 13;

    function createPaginationButton(id, text) {
        const button = document.createElement('button');
        button.classList.add('pagination-btn');
        button.id = id;
        button.textContent = text;
        return button;
    }

    const firstButton = createPaginationButton('first-page', '<<');
    const prevButton = createPaginationButton('prev-page', '<');
    paginationContainer.appendChild(firstButton);
    paginationContainer.appendChild(prevButton);

    let startPage = Math.max(1, currentPage - Math.floor(maxVisibleButtons / 2));
    let endPage = Math.min(startPage + maxVisibleButtons - 1, totalPages);

    if (endPage - startPage < maxVisibleButtons - 1) {
        startPage = Math.max(1, endPage - maxVisibleButtons + 1);
    }

    for (let page = startPage; page <= endPage; page++) {
        const pageButton = createPaginationButton(`page-${page}`, page);
        if (page === currentPage) {
            pageButton.classList.add('active');
        }
        paginationContainer.appendChild(pageButton);
    }

    const nextButton = createPaginationButton('next-page', '>');
    const lastButton = createPaginationButton('last-page', '>>');
    paginationContainer.appendChild(nextButton);
    paginationContainer.appendChild(lastButton);

    const paginationButtons = document.querySelectorAll('.pagination-btn');
    paginationButtons.forEach(button => {
        button.addEventListener('click', function () {
            const pageNumber = parseInt(button.textContent);
            if (!isNaN(pageNumber)) {
                displayPosts(pageNumber);
            } else if (button.id === 'prev-page') {
                displayPosts(currentPage - 1);
            } else if (button.id === 'next-page') {
                displayPosts(currentPage + 1);
            } else if (button.id === 'first-page') {
                displayPosts(1);
            } else if (button.id === 'last-page') {
                displayPosts(totalPages);
            }
        });
    });

    if (endPage >= maxPages) {
        lastButton.style.display = 'none';
    }
}

displayPosts(1);

window.addEventListener('scroll', function () {
    const parallax = document.querySelector('.banner');
    let scrollPosition = window.pageYOffset;

    parallax.style.backgroundPositionY = scrollPosition * 0.7 + 'px';
});

const header = document.getElementById('main-header');

window.addEventListener('scroll', function () {
    if (window.scrollY > 0) {
        header.classList.add('scrolled');
    } else {
        header.classList.remove('scrolled');
    }
});

document.addEventListener('DOMContentLoaded', function () {
    const showPerPageSelect = document.getElementById('show-per-page');
    const sortBySelect = document.getElementById('sort-by');
    const postList = document.getElementById('post-list');
    const paginationInfo = document.getElementById('pagination-info');

    let currentPage = 1;
    let itemsPerPage = parseInt(showPerPageSelect.value);
    let sortBy = sortBySelect.value;
    let totalPosts = 0;
    let posts = [];

    function fetchPosts(page, perPage, sort) {
        let dummyData = [
            { imgSrc: "/project/img/box1.jpg", date: "5 SEPTEMBER 2020", title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers" },
            { imgSrc: "/project/img/box2.jpg", date: "5 SEPTEMBER 2020", title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer ..." },
        ];

        if (sort === '-published_at') {
            dummyData.sort((a, b) => new Date(b.date) - new Date(a.date));
        } else {
            dummyData.sort((a, b) => new Date(a.date) - new Date(b.date));
        }

        totalPosts = dummyData.length;
        let startIndex = (page - 1) * perPage;
        let endIndex = startIndex + perPage;
        let paginatedPosts = dummyData.slice(startIndex, endIndex);

        return paginatedPosts;
    }

    function renderPosts(posts) {
        postList.innerHTML = '';

        posts.forEach(post => {
            const truncatedTitle = truncateText(post.title);
            const postHTML = `
                <div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0">
                    <div class="custom-block-wrap">
                        <img src="${post.imgSrc}" class="custom-block-image img-fluid lazyload"
                            alt="${post.title}">
                        <div class="custom-block">
                            <div class="custom-block-body">
                                <p class="mb-3">${post.date}</p>
                                <h6 class="title">${truncatedTitle}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            `;
            postList.innerHTML += postHTML;
        });

        const lazyLoadInstance = new LazyLoad({
            elements_selector: '.lazyload'
        });
    }

    function truncateText(text) {
        const maxLength = 100;
        if (text.length > maxLength) {
            return text.slice(0, maxLength) + '...';
        }
        return text;
    }

    function updatePaginationInfo() {
        let startIndex = (currentPage - 1) * itemsPerPage + 1;
        let endIndex = Math.min(startIndex + itemsPerPage - 1, totalPosts);
        paginationInfo.textContent = `Showing ${startIndex} - ${endIndex} of ${totalPosts}`;
    }

    function initialLoad() {
        posts = fetchPosts(currentPage, itemsPerPage, sortBy);
        renderPosts(posts);
    }

    initialLoad();

    showPerPageSelect.addEventListener('change', function () {
        itemsPerPage = parseInt(this.value);
        currentPage = 1;
        posts = fetchPosts(currentPage, itemsPerPage, sortBy);
        renderPosts(posts);
    });

    sortBySelect.addEventListener('change', function () {
        sortBy = this.value;
        currentPage = 1;
        posts = fetchPosts(currentPage, itemsPerPage, sortBy);
        renderPosts(posts);
    });

    document.addEventListener('click', function (event) {
        if (event.target.classList.contains('pagination-btn')) {
            let btnId = event.target.id;
            switch (btnId) {
                case 'first-page':
                    currentPage = 1;
                    break;
                case 'prev-page':
                    if (currentPage > 1) currentPage--;
                    break;
                case 'next-page':
                    if (currentPage < Math.ceil(totalPosts / itemsPerPage)) currentPage++;
                    break;
                case 'last-page':
                    currentPage = Math.ceil(totalPosts / itemsPerPage);
                    break;
                default:
                    currentPage = parseInt(btnId.replace('page-', ''));
                    break;
            }
            posts = fetchPosts(currentPage, itemsPerPage, sortBy);
            renderPosts(posts);
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    const postContainer = document.getElementById('post-container');
    const paginationInfo = document.getElementById('pagination-info');
    const paginationButtons = document.getElementById('pagination-buttons');
    const showPerPageSelect = document.getElementById('show-per-page');
    const sortBySelect = document.getElementById('sort-by');
    const totalPosts = 100;
    const totalPages = Math.ceil(totalPosts / 8);
    let currentPage = 1;
    let itemsPerPage = 8;

    const posts = Array.from({ length: totalPosts }, (_, i) => ({
        imgSrc: `/project/img/box${(i % 10) + 1}.jpg`,
        date: new Date(2020, 8, 5 + (i % 30)).toDateString(),
        title: `Post Title ${i + 1}`
    }));

    function renderPagination() {
        paginationButtons.innerHTML = '';
        for (let i = 1; i <= Math.ceil(totalPosts / itemsPerPage); i++) {
            const button = document.createElement('button');
            button.className = 'pagination-btn';
            button.textContent = i;
            button.addEventListener('click', function () {
                currentPage = i;
                renderPosts();
                updatePaginationInfo();
            });
            if (i === currentPage) {
                button.classList.add('active');
            }
            paginationButtons.appendChild(button);
        }
    }

    function renderPosts() {
        postContainer.innerHTML = '';
        const start = (currentPage - 1) * itemsPerPage;
        const end = start + itemsPerPage;
        const paginatedPosts = posts.slice(start, end);

        paginatedPosts.forEach(post => {
            const postElement = document.createElement('div');
            postElement.className = 'box';
            postElement.innerHTML = `
                <img src="${post.imgSrc}" alt="${post.title}">
                <p>${post.date}</p>
                <h5>${truncateText(post.title, 3)}</h5>
            `;
            postContainer.appendChild(postElement);
        });
    }

    function updatePaginationInfo() {
        const start = (currentPage - 1) * itemsPerPage + 1;
        const end = Math.min(start + itemsPerPage - 1, totalPosts);
        paginationInfo.textContent = `Showing ${start} - ${end} of ${totalPosts}`;
    }

    function truncateText(text, maxLines) {
        const lineHeight = 1.2; // assuming 1.2em line height
        const maxHeight = maxLines * lineHeight;
        const ellipsis = '...';
        let truncated = text;
        while (truncated.length > 0 && getTextHeight(truncated + ellipsis) > maxHeight) {
            truncated = truncated.slice(0, -1);
        }
        return truncated + ellipsis;
    }

    function getTextHeight(text) {
        const span = document.createElement('span');
        span.style.visibility = 'hidden';
        span.style.whiteSpace = 'nowrap';
        span.style.position = 'absolute';
        span.style.lineHeight = '1.2';
        span.textContent = text;
        document.body.appendChild(span);
        const height = span.offsetHeight / parseFloat(getComputedStyle(span).fontSize);
        document.body.removeChild(span);
        return height;
    }

    showPerPageSelect.addEventListener('change', function () {
        itemsPerPage = parseInt(this.value);
        currentPage = 1;
        renderPagination();
        renderPosts();
        updatePaginationInfo();
    });

    sortBySelect.addEventListener('change', function () {
        currentPage = 1;
        renderPosts();
    });

    document.getElementById('first-page').addEventListener('click', function () {
        currentPage = 1;
        renderPosts();
        updatePaginationInfo();
    });

    document.getElementById('prev-page').addEventListener('click', function () {
        if (currentPage > 1) {
            currentPage--;
            renderPosts();
            updatePaginationInfo();
        }
    });

    document.getElementById('next-page').addEventListener('click', function () {
        if (currentPage < Math.ceil(totalPosts / itemsPerPage)) {
            currentPage++;
            renderPosts();
            updatePaginationInfo();
        }
    });

    document.getElementById('last-page').addEventListener('click', function () {
        currentPage = Math.ceil(totalPosts / itemsPerPage);
        renderPosts();
        updatePaginationInfo();
    });

    renderPagination();
    renderPosts();
    updatePaginationInfo();
});
