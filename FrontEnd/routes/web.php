<?php

use Illuminate\Support\Facades\Route;

Route::get('work', function () {
    return view('layout.work');
});

Route::get('about', function () {
    return view('layout.about');
});

Route::get('services', function () {
    return view('layout.services');
});

Route::get('/', function () {
    return view('layout.ideas');
});

Route::get('careers', function () {
    return view('layout.careers');
});

Route::get('contact', function () {
    return view('layout.contact');
});

Route::get('/ideas', 'App\Http\Controllers\IdeasController@index');
