<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Test - Putri Tamara Gultom</title>
    <link rel="stylesheet" href="{{ asset('project/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('project/css/styles.css') }}">
</head>
<body>

    <header id="main-header">
        <div class="container">
            <div class="logo">
                <img src="{{ asset('project/img/logo.png') }}" alt="Logo" width="120px" height="60px">
            </div>
            <nav>
                <ul>
                    <li><a href="/work" class="{{ Request::is('work') ? 'active' : '' }}">Work</a></li>
                    <li><a href="/about" class="{{ Request::is('about') ? 'active' : '' }}">About</a></li>
                    <li><a href="/services" class="{{ Request::is('services') ? 'active' : '' }}">Services</a></li>
                    <li><a href="/" class="{{ Request::is('/') ? 'active' : '' }}">Ideas</a></li>
                    <li><a href="/careers" class="{{ Request::is('careers') ? 'active' : '' }}">Careers</a></li>
                    <li><a href="/contact" class="{{ Request::is('contact') ? 'active' : '' }}">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header>
