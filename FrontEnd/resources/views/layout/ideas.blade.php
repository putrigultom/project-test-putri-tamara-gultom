<!DOCTYPE html>
<html lang="en">
@include('partial.header')

<section class="banner" style="background-image: url('{{ asset('project/img/banner.jpg') }}');">
    <div class="banner-content">
        <h1>Ideas</h1>
        <p>Where all our great things begin</p>
    </div>
</section>

<div class="pagination-container">
    <div class="pagination-info">
        Showing 1 - 10 of 100
    </div>
    <div class="pagination-controls">
        <label for="show-per-page">Show per page :</label>
        <select id="show-per-page" class="rounded-select">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
        </select>
        <label for="sort-by">Sort by :</label>
        <select id="sort-by" class="rounded-select">
            <option value="-published_at">Newest</option>
            <option value="published_at">Oldest</option>
        </select>
    </div>
</div>

<div id="postList" class="container">

</div>

<div class="pagination-container-down">
    <button class="pagination-btn" id="first-page">&laquo;&laquo;</button>
    <button class="pagination-btn" id="prev-page">&laquo;</button>
    <button class="pagination-btn active" id="page-1">1</button>
    <button class="pagination-btn" id="page-2">2</button>
    <button class="pagination-btn" id="page-3">3</button>
    <button class="pagination-btn" id="page-4">4</button>
    <button class="pagination-btn" id="page-5">5</button>
    <button class="pagination-btn" id="next-page">&raquo;</button>
    <button class="pagination-btn" id="last-page">&raquo;&raquo;</button>
</div>
<script src="{{ asset('project/js/app.js') }}"></script>
</body>

</html>
