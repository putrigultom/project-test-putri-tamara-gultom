<!DOCTYPE html>
<html lang="en">
@include('partial.header')

<section class="banner" style="background-image: url('{{ asset('project/img/banner.jpg') }}');">
    <div class="banner-content">
        <h1>Services</h1>
        <p>Where all our great things begin</p>
    </div>
</section>
<script src="{{ asset('project/js/app.js') }}"></script>

</body>
</html>
